import React from 'react';

export default class FormMovie extends React.Component {
    state = {title: ''};

    onSubmit = (e) => {
        e.preventDefault();
        let item = e.target[0].value;
        if (!item) console.log('error');
        else {
            this.props.onSubmit(item);
             this.setState({task: ''})}
    }

    onChange = (e) => this.setState({ title: e.target.value });

    render() {

        return (
            <form onSubmit={this.onSubmit} style={{width:'530px',display:'flex', flexDirection: 'row', justifyContent: 'space-around', margin: '0 auto'}}>
                <input 
                    type="text" 
                    name="title"
                    style={{width: '500px', padding: '5px'}}
                    placeholder="Add Title(m/s)..." 
                    value={this.state.title}
                    onChange={this.onChange}
                />
                <button type="submit" > add </button>
            </form>
        )
    }
}